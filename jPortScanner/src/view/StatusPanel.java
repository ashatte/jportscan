package view;

import javax.swing.JLabel;
import javax.swing.JPanel;

import model.PortScanner;

public class StatusPanel extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4293509344275294192L;
	private JLabel durationlabel;
	
	public StatusPanel() {
		durationlabel = new JLabel("");
		add(durationlabel);
		setVisible(false);
	}
	
	public void showCounter(boolean isVisible) {
		setVisible(isVisible);
	}
	
	public void updateLabel() {
		durationlabel.setText("Found " + PortScanner.openPorts.size() + " open ports.");
	}
	
	public void updateTimeElapsed() {
		
	}

}
