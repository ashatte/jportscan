package view;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JFrame;

import config.Config;

public class MainFrame extends JFrame {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 74706806156754312L;
	public final static Dimension FRAME_DIMENSIONS = new Dimension(250,  170);

	public MainFrame() {
		setSize(FRAME_DIMENSIONS);
		setTitle(Config.APP_TITLE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new GridLayout(4, 1));
		setResizable(false);
		setLocationRelativeTo(null);
	}	
}
