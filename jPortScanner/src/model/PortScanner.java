package model;

import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.HashMap;

public class PortScanner {
		
	public static HashMap<Integer, Boolean> openPorts = new HashMap<Integer, Boolean>();
	
	public static boolean isPortOpen(String ip, int port, int timeout) {
        try {
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress(ip, port), timeout);
            socket.close();
            System.out.println(ip + ", port:" + port + " is open");
            openPorts.put(port, true);
            return true;
        } catch (Exception ex) {
        	System.out.println(ip + ", port:" + port + " not open");
            return false;
        }
    }
	
    public static void scanWithThreads(final String ip, int portMin, int portMax) {
    	openPorts.clear();
    	
    	int numThreads = ((portMax - portMin) / 500) + 1;
    	
    	for (int i=0; i < numThreads; i++) {
    		final int currentMin = (i * 500) + portMin;
    		int tempMax = currentMin + 500;
    		final int currentMax = tempMax > portMax ? portMax : tempMax;
    		
    		Runnable r = new Runnable() {
				
				@Override
				public void run() {
					for (int i = currentMin; i <= currentMax; i++) {
						isPortOpen(ip, i, 10);
					}
				}
			};
			
			new Thread(r).start();
    	}
    }

}
