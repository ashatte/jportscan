package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

import model.PortScanner;
import view.MainFrame;
import view.StatusPanel;

public class App {

	private MainFrame mainFrame;
	private JLabel ipLabel, portLabel, sepLabel;
	private JPanel ipPanel, portPanel, bttnPanel;
	private StatusPanel statusPanel;
	private JTextField ipField1, ipField2, ipField3, ipField4, portFieldMin,
			portFieldMax;
	private JButton executeBttn;

	public App() {
		mainFrame = new MainFrame();

		initializeIPFields();

		portPanel = new JPanel();
		portLabel = new JLabel("Port range: ");
		sepLabel = new JLabel(" - ");
		portFieldMin = new JTextField(5);
		portFieldMax = new JTextField(5);
		portPanel.add(portLabel);
		portPanel.add(portFieldMin);
		portPanel.add(sepLabel);
		portPanel.add(portFieldMax);

		statusPanel = new StatusPanel();

		bttnPanel = new JPanel();
		executeBttn = new JButton("Start Scanning");
		executeBttn.addActionListener(buttonListener);
		bttnPanel.add(executeBttn);

		mainFrame.add(ipPanel);
		mainFrame.add(portPanel);
		mainFrame.add(statusPanel);
		mainFrame.add(bttnPanel);
		
		setupJMenuBar();

		mainFrame.setVisible(true);
	}

	private void initializeIPFields() {
		KeyListener validation = new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				JTextField source = (JTextField) e.getSource();
				String input = source.getText();
				if (source.getText().length() > 2) {
					source.setText(input.substring(0, 2));
				}
			}

			@Override
			public void keyReleased(KeyEvent arg0) {

			}

			@Override
			public void keyPressed(KeyEvent arg0) {

			}
		};

		ipPanel = new JPanel();
		ipLabel = new JLabel("IP address: ");
		ipField1 = new JTextField(2);
		ipField1.addKeyListener(validation);
		ipField2 = new JTextField(2);
		ipField2.addKeyListener(validation);
		ipField3 = new JTextField(2);
		ipField3.addKeyListener(validation);
		ipField4 = new JTextField(2);
		ipField4.addKeyListener(validation);
		ipPanel.add(ipLabel);
		ipPanel.add(ipField1);
		ipPanel.add(new JLabel("."));
		ipPanel.add(ipField2);
		ipPanel.add(new JLabel("."));
		ipPanel.add(ipField3);
		ipPanel.add(new JLabel("."));
		ipPanel.add(ipField4);

	}
	
	private Timer updateGUITimer = new Timer(1, new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			statusPanel.updateLabel();
		}
	});
	
	private ActionListener buttonListener = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			switch (e.getActionCommand()) {
			case "Start Scanning":
				if (validateInput()) {
					String ipAddress = ipField1.getText() + "." + ipField2.getText() + "." + ipField3.getText() + "." + ipField4.getText();
					PortScanner.scanWithThreads(ipAddress, Integer.parseInt(portFieldMin.getText()), Integer.parseInt(portFieldMax.getText()));
					statusPanel.showCounter(true);
					updateGUITimer.start();
				}
				else System.out.println("not valid");
				break;
			}
		}

		private boolean validateInput() {
			try {
				Integer.parseInt(ipField1.getText());
				Integer.parseInt(ipField2.getText());
				Integer.parseInt(ipField3.getText());
				Integer.parseInt(ipField4.getText());
				Integer.parseInt(portFieldMin.getText());
				Integer.parseInt(portFieldMax.getText());
			} catch (Exception e) {
				System.out.println(e.getMessage());
				return false;
			}
			return true;
		}
	};

	private void setupJMenuBar() {
		JMenuBar menubar = new JMenuBar();
		JMenu menu = new JMenu();

		menubar.add(menu);

		mainFrame.setJMenuBar(menubar);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				new App();
			}
		});
	}
}
