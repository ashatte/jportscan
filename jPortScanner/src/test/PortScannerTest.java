package test;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

import model.PortScanner;

public class PortScannerTest {
	
	private boolean testHasEnded = false;
	
	public PortScannerTest() {
		final String ip = "103.29.84.168";
		final int minPort = 0;
		final int maxPort = 65000;
		
		final int TIMEOUT = 10;
		
		testTimer.start();
		for (int i = minPort; i <= maxPort; i++) {
			System.out.print("Port " + i + ": ");
			System.out.println(PortScanner.isPortOpen(ip, i, TIMEOUT));
		}
		testHasEnded = true;
	}
	
	public static void main(String[] args) {
		new PortScannerTest();
	}
	
	private int totalTime = 0;
	private Timer testTimer = new Timer(1000, new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (!testHasEnded) totalTime++;
			else {
				testTimer.stop();
				System.out.println("All tests completed in " + totalTime + " seconds.");
			}
		}
	});

}
